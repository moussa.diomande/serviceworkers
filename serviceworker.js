self.addEventListener(type: 'install', listener: function (event: Event)) {
    event.waitUntil(
        caches.open(cacheName: "my-cache")
    .then(function (cache: Cache) {
            return cache.addAll(
                request:[
                    '/web_mobile/listing.html',
                    '/web_mobile/css/style-list.css',
                    '/web_mobile/javascript/app.js',
                    '/web_mobile/assets/favicon.ico',
                    '/web_mobile/assets/placeholder.png',
                    'https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2',
                    'https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5fBBc4.woff2',
                    'https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu72xKOzY.woff2',
                    'https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu4mxK.woff2'
        ]
            )

        })
    )

})
self.addEventListener(type: 'fetch', listener: function(event: Event)
{
    //console.log(event.request);
    let pictureRegexp = /https:\/\/picsum.photos\/(.)*/;

    if (event.request.url.match(pictureRegexp)){
        event.respondWith(
            fetch(event.request)
                .catch((error) => ) {
                return caches.match(request:'/web_mobile/assets/placeholder.png')

        })
    )
    } else {
    if (event.respondWith(
    caches.match(event.request)
        .then(function (response:Response|undefined){
        if (response)
            return response;
        return fetch(event.request)
})

)
    }
})